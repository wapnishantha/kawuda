=== Kawuda UTM source tracker ===

Contributors: wapnishantha
Tags: Kawuda, Kawuda UTM source tracker system
Requires at least: 5.0 
Tested up to: 5.8
Stable tag: 1.0.0 
License: GPLv2 or later License URI: http://www.gnu.org/licenses/gpl-2.0.html


Kawuda is a simple UTM source tracking system. No need depend on 3rd party. You can use this is as your own anatlatic system

=== Description ===

Kawuda is simple UTM source tracking system. No need depends on an 3rd party. This is a not only utm tracker but also simple analytic system. Kawuda system will tack the use events. Right now it is facilitating to track a link and button. On there, You can search utm source. There are dates filter too.


With the latest implementation of the intent, your marking ROI can not be analysis without correct data. Some browser will not allow the 3rd party cookies. Then your analytic data is totally wrong in your 3rd party analytic system. But Kawuda will support you to check and analysis your UTM source, UTM medium etc

Kawuda is also support for your normal user tacking. So, This is kind of Wordpress system log. If you want to flush all the current data in Kawuda system just deactivate and activate.

=== Plugin Features ===

* Free!
* simple and fast
* Dashboard 
* Unlimited tacking
* Real time tacking stat 
* Search
* Date filter
* Admin user task track
* Other user task track


You can browse the code at the [bitbucket repository](https://bitbucket.org/wapnishantha/kawuda).

=== Installation ===

1. Unpack archive to this archive to the 'wp-content/plugins/' directory inside of WordPress
2. Activate the plugin through the 'Plugins' menu in WordPress

OR

1. Log in to WordPress admin panel for your WordPress site. Click on 'Plugins > Add New'.
2. Search for 'Kawuda UTM source tracker' and install.
3. Once installed, click on 'Activate' to enable plugin.

Step to setup
1. Go to setting page and setup no of recode you need to show

=== Screenshots ===

 

=== Changelog ===

* No change log fresh plugin

=== Support ===

* Email support wapnishantha@gmail.com
